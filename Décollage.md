============= 
Préparation au décollage ==============
=============
_Allumer la radiocommande et baisser le stick de Thrust (poussée) au minimum

_Mode Angle : SB == milieu (mode débutant, limite d'angle)

_PreArm Switch : SD == bas

_ARM Switch: SA == bas (pour désarmer)

_Vérifier l'état de la batterie

_Eventuellement calibrer l'accéléromètre sur Betaflight en plaçant le drone sur surface plane.

=============
Décollage du drone ==============
=============

_Mode Angle : SB == milieu (mode débutant, limite d'angle)

_Mode Horizon : SB == bas (mode débutant sans limite d'angle, non recommandé)

_PreArm Switch : SD == haut 

_ARM Switch: SA == haut (pour armer)




