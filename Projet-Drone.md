E3 uwu


Projet : Drone
============

**Introduction :**

Le but de ce projet est de créer un drone à partir de la base imprimée à l’école.
Ce projet est le moins abouties, mais c’est l’occasion de laisser court à votre imagination.

Année 2022/2023 : 

Durant cette année, nous avons acheté un contrôleur de vol du commerce et démarrer la configuration de ce dernier. Nous avons également pu concevoir et imprimer les pieds.

Année 2023/2024 : 

Le drone est prêt à prendre son envol. Mais avant ça, nous allons concevoir un banc de test et effectuer les derniers petits réglages avant le premier vol. 
Nous allons également ajouté d'autres fonctionnalités au drone : caméra, GPS...

